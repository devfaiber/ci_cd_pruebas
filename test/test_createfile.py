from src import create
import os

def test_create():
    folder = "files"
    filename = create.info(folder)

    path = os.path.join(os.path.dirname(__file__), '..', folder, filename)

    assert os.path.exists(path) == True